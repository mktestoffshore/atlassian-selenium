package com.atlassian.webdriver.jira;

import com.atlassian.pageobjects.MultiTenantTestedProduct;
import com.atlassian.webdriver.testing.BaseTestProductTest;
import org.junit.Before;

public class TestJiraTestedProduct extends BaseTestProductTest {

    private static final String HOMEPAGE_PATH = "secure/Dashboard.jspa";

    private JiraTestedProduct jiraTestedProduct;

    @Before
    public void setup() {
        super.setup();
        jiraTestedProduct = new JiraTestedProduct(testerFactory, productInstance);
    }

    @Override
    protected void goToHomePage() {
        jiraTestedProduct.gotoHomePage();
    }

    @Override
    protected String getHomePagePath() {
        return HOMEPAGE_PATH;
    }

    @Override
    protected MultiTenantTestedProduct getProduct() {
        return jiraTestedProduct;
    }

}
